package longpoll;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;


@RestController
public class Controller {

    private final AtomicInteger counter = new AtomicInteger(0);
    private final AtomicInteger counter2 = new AtomicInteger(0);

    private volatile CompletableFuture<Integer> pendingResult;

    public Controller() {
        new Thread(() -> {
            while (true) {
                counter2.incrementAndGet();
                if (pendingResult != null) {
                    pendingResult.complete(counter2.get());
                    pendingResult = null;
                }
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @RequestMapping("/counter")
    public int counter() {
        return counter.getAndIncrement();
    }

    @RequestMapping("/counter2")
    public CompletableFuture<Integer> counter2() {
        pendingResult = new CompletableFuture<>();
        return pendingResult;
    }
}
