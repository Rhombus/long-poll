angular.module('poll', [])
.controller('main', function($scope, $http) {

    var getCounter = function() {
        $http.get('http://localhost:8080/counter').
            then(function(response) {
                $scope.counter = response.data;
        });
        setTimeout(getCounter, 1000)
    }

    var getCounter2 = function() {
        $http.get('http://localhost:8080/counter2').
            then(function(response) {
                $scope.counter2 = response.data;
                setTimeout(getCounter2, 0)
        });
    }

    getCounter()
    getCounter2()
});